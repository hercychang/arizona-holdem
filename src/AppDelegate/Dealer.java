package AppDelegate;

/**
 * @author Collin Gifford
 * @author YUNHAO ZHANG
 */

import java.text.NumberFormat;
import java.util.Collections;
import java.util.ArrayList;

import CardModel.Card;
import CardModel.Rank;
import CardModel.Suit;

/*
 * Manages the game logic for a game of Arizona Hold Em. Maintains a shuffled deck of 52 Poker cards;
 * Manages a number of players, dealing them cards and showing them the community cards available;
 * The Deal method encapsulates the totality of the procedure for a single hand of poker, and returns a string describing it.
 */
public class Dealer
{

	private Card[] communityCards;
	private ArrayList<Card> deck;
	private Player[] players;
	private double pot;
	private boolean showAll;

	public double getPot()
	{
		return pot;
	}

	public Dealer(Player[] allPlayers)
	{
		deck = new ArrayList<Card>();
		for (Rank rank : Rank.values())
		{
			for (Suit suit : Suit.values())
			{
				deck.add(new Card(rank, suit));
			}
		}
		this.players = allPlayers;
		communityCards = new Card[5];

		this.pot = 0.0;
		this.showAll = false;

	}

	public boolean getShowAll()
	{
		return showAll;
	}

	public void setShowAll(boolean showAll)
	{
		this.showAll = showAll;
	}

	/* 1 */
	public ArrayList<PokerHand> getWinningHands()
	{
		ArrayList<PokerHand> allHands = new ArrayList<PokerHand>();
		for (Player player : players)
		{
			if (!(player.hasFolded()))
				allHands.add(player.getBestHand(communityCards));
		}
		Collections.sort(allHands);
		ArrayList<PokerHand> winningHands = new ArrayList<PokerHand>();
		for (int i = allHands.size() - 1; i >= 0; i--)
		{
			if (allHands.get(allHands.size() - 1).compareTo(allHands.get(i)) == 0)
			{
				winningHands.add(allHands.get(i));
			}
		}
		return winningHands;
	}

	public void setCommunityCards(Card[] inputCommunityCards)
	{
		communityCards = inputCommunityCards;
	}

	public ArrayList<Player> getWinners()
	{
		ArrayList<PokerHand> winningHands = getWinningHands();
		ArrayList<Player> winners = new ArrayList<Player>();
		for (int i = 0; i < players.length; i++)
		{
			for (int j = 0; j < winningHands.size(); j++)
			{
				if (players[i].getBestHand(communityCards).equals(
						winningHands.get(j)))
				{
					winners.add(players[i]);
				}
			}
		}
		return winners;
	}

	public boolean isWinner(Player player)
	{
		ArrayList<Player> winners = getWinners();
		for (int i = 0; i < winners.size(); i++)
		{
			if (winners.get(i).equals(player))
			{
				return true;
			}
		}
		return false;
	}

	public void awardWinnings(double pot, ArrayList<Player> winners)
	{
		for (Player player : winners)
		{
			player.deposit(pot / winners.size());
		}

		this.pot = 0;
	}

	public double collectAnte()
	{
		double money = 0;
		for (Player player : this.players)
		{
			money += player.ante();
			player.setFolded(false);
		}
		this.pot += money;

		return money;
	}

	public void addToPot(double amount)
	{
		this.pot += amount;
	}

	public Player[] getPlayers()
	{
		return players.clone();
	}

	public void preBetDeal()
	{
		this.showAll = false;
		this.collectAnte();
		Collections.shuffle(deck);
		int index = 0;

		for (int i = 0; i < players.length; i++)
		{

			players[i].dealHoleCards(deck.get(index), deck.get(index + 1));
			players[i].setFolded(false);
			index += 2;
		}

		communityCards[0] = deck.get(index++);
		communityCards[1] = deck.get(index++);
		communityCards[2] = deck.get(index++);
		communityCards[3] = deck.get(index++);
		communityCards[4] = deck.get(index++);

	}

	public void postBetDeal()
	{
		this.showAll = true;
		this.awardWinnings(pot, this.getWinners());

	}

	@Deprecated
	public String deal()
	{
		Collections.shuffle(deck);

		String output = "Community Cards:";

		this.collectAnte();
		int index = 0;

		for (int i = 0; i < players.length; i++)
		{

			players[i].dealHoleCards(deck.get(index), deck.get(index + 1));
			index += 2;
		}

		communityCards[0] = deck.get(index++);
		communityCards[1] = deck.get(index++);
		communityCards[2] = deck.get(index++);
		communityCards[3] = deck.get(index++);
		communityCards[4] = deck.get(index++);

		ArrayList<PokerHand> winningHands = getWinningHands();
		ArrayList<Player> winners = getWinners();

		for (int i = 0; i < communityCards.length; i++)
		{
			output = output + " " + communityCards[i].toString();
		}

		output = output + "\n";
		for (int i = 0; i < players.length; i++)
		{
			output = output + players[i].toString();
			output = output + "\n      Best hand:"
					+ players[i].getBestHand(communityCards) + "\n";
		}
		output = output + "Winning hand";
		if (winningHands.size() > 1)
		{
			output = output + "s (tie)";
		}
		output = output + "\n";

		awardWinnings(pot, winners);

		for (int i = 0; i < winners.size(); i++)
		{

			NumberFormat moneyFormat = NumberFormat.getCurrencyInstance();
			String money = moneyFormat.format(winners.get(i).getBalance());
			String winningHand = winners.get(i).getBestHand(communityCards)
					.toString().substring(1);
			output = output + winningHand + " " + winners.get(i).getName()
					+ " " + money + "\n";
		}

		return output;
	}

	public Card[] getCommunalCards()
	{
		return communityCards.clone();
	}

}
