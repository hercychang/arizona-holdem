package AppDelegate;
/**
 * @author Collin Gifford
 * @author YUNHAO ZHANG
 */
import java.text.NumberFormat;
import java.util.Collections;
import java.util.HashSet;
import java.util.ArrayList;

import CardModel.Card;


/*
 * Represents a player in a game of Arizona Hold Em. Has two hole cards, and when given 5 community cards can deduce their strongest available hand.
 */

public class Player
{

	private static final double INITIAL_BALANCE = 100.0;
	private static final double ANTE_AMOUNT = 2.00;
	private String name;
	private double balance;
	private Card[] holeCards;
	private boolean folded;
	

	public boolean hasFolded() {
		return folded;
	}

	public void setFolded(boolean folded) {
		this.folded = folded;
	}

	public Player(String name)
	{

		this.name = name;
		this.balance = INITIAL_BALANCE;
		this.holeCards = new Card[2];
		this.folded = false;
	}

	public double ante()
	{
		this.balance -= ANTE_AMOUNT;
		return ANTE_AMOUNT;
	}
	
	public double getAnte()
	{
		return ANTE_AMOUNT;
	}

	public double getBalance()
	{

		return this.balance;
	}

	public void deposit(double winnings)
	{
		this.balance += winnings;

	}

	public void dealHoleCards(Card card, Card card2)
	{
		this.holeCards[0] = card;
		this.holeCards[1] = card2;

	}

	public Card[] getHoleCards()
	{
		return this.holeCards.clone();
	}

	public PokerHand getBestHand(Card[] community)
	{
		HashSet<PokerHand> cardsSet = new HashSet<PokerHand>();
		Card[] allCards = new Card[7];

		allCards[0] = holeCards[0];
		allCards[1] = holeCards[1];

		for (int i = 2; i < allCards.length; i++)
		{
			allCards[i] = community[i - 2];
		}

		for (int i = 0; i < allCards.length; i++)
		{
			for (int j = 0; j < allCards.length; j++)
			{
				ArrayList<Card> temp = new ArrayList<Card>();
				if (i != j)
				{
					for (int picker = 0; picker < allCards.length; picker++)
					{
						if (picker != i && picker != j)
						{
							temp.add(allCards[picker]);
						}
					}
					cardsSet.add(new PokerHand(temp.get(0), temp.get(1), temp
							.get(2), temp.get(3), temp.get(4)));
				}
			}
		}

		ArrayList<PokerHand> sortedHands = new ArrayList<PokerHand>(cardsSet);
		Collections.sort(sortedHands);

		return sortedHands.get(sortedHands.size() - 1);
	}

	public String getName()
	{
		return this.name;
	}

	public String toString()
	{
		NumberFormat moneyFormat = NumberFormat.getCurrencyInstance();
		String money = moneyFormat.format(balance);

		return name + "\t" + money + " - " + holeCards[0].toString() + " "
				+ holeCards[1].toString();
	}

	public void take(double amount) {
		this.balance -= amount;
		
	}
}
