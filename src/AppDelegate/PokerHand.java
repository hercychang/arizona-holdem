package AppDelegate;
/**
 * @author Collin Gifford
 * @author YUNHAO ZHANG
 */

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.ListIterator;

import CardModel.Card;
import CardModel.Rank;


/*
 * Represents a hand of exactly five cards from a standard 52-card poker deck.
 * Functionality is provided for comparing two hands to determine which is the stronger hand.
 * Uniqueness of particular cards is enforced both within the hand and between two compared hands.
 */
public class PokerHand implements Comparable<PokerHand>
{
	enum HandType
	{
		HighCard(0), Pair(1), TwoPair(2), ThreeOfAKind(3), Straight(4), Flush(5), FullHouse(
				6), FourOfAKind(7), StraightFlush(8);

		int value;

		HandType(int val)
		{
			this.value = val;
		}

		public int getValue()
		{
			return this.value;
		}

	}

	private ArrayList<Card> hand;

	private HandType type;

	public PokerHand(Card first, Card second, Card third, Card fourth,
			Card fifth)
	{
		this.hand = new ArrayList<Card>();
		hand.add(first);
		testUniqueness(second);
		hand.add(second);
		testUniqueness(third);
		hand.add(third);
		testUniqueness(fourth);
		hand.add(fourth);
		testUniqueness(fifth);
		hand.add(fifth);
		Collections.sort(hand);
		this.classifyHandType();
	}

	private void classifyHandType()
	{
		// Rather than check for straightness and flushness twice, keep track of
		// it the first time through
		boolean isStraight = false;
		boolean isFlush = false;
		Iterator<Card> iter = hand.listIterator();
		Card current = iter.next();
		// Test for a straight
		while (iter.hasNext())
		{
			Card next = iter.next();
			// Check if the next highest ranked card is exactly one rank above,
			// Or if it is an Ace completing a straight of 2-3-4-5
			// If not, stop checking. If the end of the hand is reached, it's a
			// straight
			if ((next.getRank().getValue()) - (current.getRank().getValue()) == 1
					|| (current.getRank() == Rank.Five && next.getRank() == Rank.Ace))
			{
				if (!(iter.hasNext()))
				{
					isStraight = true;
				}

				current = next;

			} else
				break;

		}
		iter = hand.listIterator();
		current = iter.next();
		// Test for a flush
		while (iter.hasNext())
		{
			Card next = iter.next();
			// Check if the next card is the same suit as the current card
			// If not, stop checking. If the end of the hand is reached, it's a
			// flush
			if (current.getSuit() == next.getSuit())
			{
				if (!(iter.hasNext()))
				{
					isFlush = true;
				}
				current = next;
			}else
				break;
		}

		// To avoid excess work, check for a straight flush immediately
		// Classify straight flushes, exit if confirmed
		if (isStraight && isFlush)
		{
			this.type = HandType.StraightFlush;
			return;
		}

		// Test for multiple cards of the same rank;
		// Track ranked quantities in a hashmap, to be consulted when
		// classifying the hands
		HashMap<Rank, Integer> countMap = new HashMap<Rank, Integer>();
		iter = hand.listIterator();

		while (iter.hasNext())
		{
			current = iter.next();
			Rank rankCache = current.getRank();
			if (countMap.containsKey(rankCache))
			{
				countMap.put(rankCache, countMap.get(rankCache) + 1);
			}
			else
			{
				countMap.put(rankCache, 1);
			}
		}

		// Store the counts as a variable to avoid repeated lookup
		Collection<Integer> counts = countMap.values();
		// Classify four of a kind, exit if confirmed
		if (counts.contains(4))
		{
			this.type = HandType.FourOfAKind;
			return;
		}

		// Classify full house, exit if confirmed
		if (counts.contains(3) && counts.contains(2))
		{
			this.type = HandType.FullHouse;
			return;
		}

		// Classify Flush, exit if confirmed
		if (isFlush)
		{
			this.type = HandType.Flush;
			return;
		}

		// Classify Straight, exit if confirmed
		if (isStraight)
		{
			this.type = HandType.Straight;
			return;
		}

		// Classify Three of a kind, exit if confirmed
		if (counts.contains(3))
		{
			this.type = HandType.ThreeOfAKind;
			return;
		}

		// Test for Pairs
		// First, a boolean checking whether any pairs exist is determined
		// Then, one pair is removed from the count in order to check for a
		// two-pair
		// If no second pair exists, the boolean is evidence of the removed pair
		// count
		boolean hasPair = counts.contains(2);
		// Classify Two pair, exit if confirmed
		// To confirm that there are two counts of 2, one count is removed from
		// the collection

		if (hasPair)
		{
			counts.remove(2);
			if (counts.contains(2))
			{
				this.type = HandType.TwoPair;
				return;
			}
		}

		// Classify Pair, exit if confirmed
		if (hasPair)
		{
			this.type = HandType.Pair;
			return;
		}

		// Classify High Card
		this.type = HandType.HighCard;

	}

	@Override
	public int compareTo(PokerHand target)
	{
		/*
		 * // Ensure no duplicate cards for (int i = 0; i < 5; i++) { for (int j
		 * = 0; j < 5; j++) { if (this.hand.get(i).getRank() ==
		 * target.hand.get(j).getRank() && this.hand.get(i).getSuit() ==
		 * target.hand.get(j) .getSuit()) { throw new DuplicateCardException();
		 * } } }
		 */

		// Compare hand types, hoping for quick exit
		if (this.type.getValue() < target.type.getValue())
		{
			return -1;
		}
		if (this.type.getValue() > target.type.getValue())
		{
			return 1;
		}
		// Hand types are the same; determine which hand of that type is
		// stronger
		switch (this.type)
		{
		// Straights are anomalous in that Aces might be low in order to make
		// the straight, and so are checked separately
			case Straight:
			case StraightFlush:

				// Check both hands to see if they are an ace-low straight;
				// Record the high-card value for comparison
				int effectiveThisRank,
				effectiveTargetRank;
				if (this.hand.get(4).getRank() == Rank.Ace
						&& this.hand.get(3).getRank() == Rank.Five)
				{
					effectiveThisRank = Rank.Five.getValue();
				}
				else
				{
					effectiveThisRank = this.hand.get(4).getRank().getValue();
				}
				if (target.hand.get(4).getRank() == Rank.Ace
						&& target.hand.get(3).getRank() == Rank.Five)
				{
					effectiveTargetRank = Rank.Five.getValue();
				}
				else
				{
					effectiveTargetRank = target.hand.get(4).getRank()
							.getValue();
				}
				// Compare effective highcard value to rank straights
				if (effectiveThisRank > effectiveTargetRank)
				{
					return 1;
				}
				else if (effectiveThisRank < effectiveTargetRank)
				{
					return -1;
				}
				else
				{
					return 0;
				}

			default:
				// Create two lists of the ranks contained in each hand,
				// Sorted primarily by the quantity of cards of that rank,
				// and secondarily by the value of the ranks
				ArrayList<Rank> thisRanks = this.getQuantizedRanks();
				ArrayList<Rank> targetRanks = target.getQuantizedRanks();
				for (int i = thisRanks.size() - 1; i >= 0; i--)
				{
					if (thisRanks.get(i).compareTo(targetRanks.get(i)) > 0)
					{
						return 1;
					}
					else if (thisRanks.get(i).compareTo(targetRanks.get(i)) < 0)
					{
						return -1;
					}

				}
		}
		return 0;
	}

	private ArrayList<Rank> getQuantizedRanks()
	{
		ArrayList<Rank> quantizedList = new ArrayList<Rank>(5);
		Iterator<Card> iter = this.hand.listIterator();
		// Count how many cards of each rank are in the hand
		HashMap<Rank, Integer> rankCounts = new HashMap<Rank, Integer>();
		while (iter.hasNext())
		{
			Card current = iter.next();
			if (rankCounts.containsKey(current.getRank()))
			{
				rankCounts.put(current.getRank(),
						rankCounts.get(current.getRank()) + 1);
			} else
			{
				rankCounts.put(current.getRank(), 1);
			}
		}
		// Consult the table of rank quantities, checking for increasing
		// quantities;
		// When a pair or single card is found, put it aside in a temporary
		// array
		// Sort the grouped cards based on the magnitude of the ranks
		// There can only be one quadruple or triplet, so there is no need to
		// sort those
		// Add these sorted ranks into an ArrayList
		ArrayList<Rank> singletonCards = new ArrayList<Rank>(5);
		for (Rank queriedRank : rankCounts.keySet())
		{
			if (rankCounts.get(queriedRank) == 1)
			{
				singletonCards.add(queriedRank);
			}
		}
		Collections.sort(singletonCards);
		quantizedList.addAll(singletonCards);
		ArrayList<Rank> dualCards = new ArrayList<Rank>(2);
		for (Rank queriedRank : rankCounts.keySet())
		{
			if (rankCounts.get(queriedRank) == 2)
			{
				dualCards.add(queriedRank);
			}
		}
		Collections.sort(dualCards);
		quantizedList.addAll(dualCards);

		for (Rank queriedRank : rankCounts.keySet())
		{
			if (rankCounts.get(queriedRank) == 3)
			{
				quantizedList.add(queriedRank);
			}
		}
		for (Rank queriedRank : rankCounts.keySet())
		{
			if (rankCounts.get(queriedRank) == 4)
			{
				quantizedList.add(queriedRank);
			}
		}
		return quantizedList;
	}

	// Compares a Card to all Cards in an existing ArrayList to check for
	// duplication
	private void testUniqueness(Card candidate)
	{
		ListIterator<Card> iter = hand.listIterator();
		while (iter.hasNext())
		{
			Card member = iter.next();
			if (candidate.getRank() == member.getRank()
					&& candidate.getSuit() == member.getSuit())
			{
				// throw new DuplicateCardException();
			}
		}

	}

	@Override
	public String toString()
	{
		String stringOfCards = "";

		ArrayList<Card> sortedCards = sortingIntoOutputFormat(hand);

		for (int i = 0; i < sortedCards.size(); i++)
		{
			stringOfCards = stringOfCards + " " + sortedCards.get(i).toString();
		}

		return stringOfCards;
	}

	private ArrayList<Card> sortingIntoOutputFormat(
			ArrayList<Card> unSortedCards)
	{
		ArrayList<Card> sortedCards = new ArrayList<Card>(unSortedCards);
		if (type.equals(HandType.Straight)
				|| type.equals(HandType.StraightFlush))
		{
			if (hand.get(hand.size() - 1).getRank().equals(Rank.Ace)
					&& hand.get(0).getRank().equals(Rank.Deuce))
			{
				sortedCards = new ArrayList<Card>();
				sortedCards.add(hand.get(hand.size() - 1));
				sortedCards.add(hand.get(0));
				sortedCards.add(hand.get(1));
				sortedCards.add(hand.get(2));
				sortedCards.add(hand.get(3));
			}
		}
		return sortedCards;
	}

	@Override
	public int hashCode()
	{
		int hash = 0;
		int i = 0;
		for (Card card : this.hand)
		{
			hash = ((hash ^ (card.hashCode() | i)) << i) ^ (card.hashCode());
			i++;
		}

		return hash;

	}

	@Override
	public boolean equals(Object o)
	{
		if (o.getClass() == this.getClass())
		{

			if (((PokerHand) o).hand.containsAll(this.hand))
			{
				return true;
			} else
			{
				return false;
			}

		} else
		{
			return false;
		}

	}

	public ArrayList<Card> toArrayList()
	{
		return new ArrayList<Card>(hand);
	}
}
