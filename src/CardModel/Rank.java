package CardModel;
/**
 * @author Collin Gifford
 * @author YUNHAO ZHANG
 */

/*
 * Represents the rank value of a card from a standard 52-card poker deck
 * May have a value representing a 2, 3, 4, 5, 6, 7, 8, 9, 10, Jack, Queen, King, or Ace
 */
public enum Rank
{
	Deuce(2), Three(3), Four(4), Five(5), Six(6), Seven(7), Eight(8), Nine(9), Ten(10), Jack(11), Queen(12), King(13), Ace(14);

	private int value;

	private Rank(int val)
	{
		this.value = val;

	}

	public int getValue()
	{
		return this.value;
	}

}