package CardModel;
/**
 * @author Collin Gifford
 * @author YUNHAO ZHANG
 */

/*
 * Represents a suit of cards from a standard 52-card poker deck
 * May have a value representing the suit of diamonds, clubs, hearts, or spades
 */

public enum Suit
{
	Clubs(1), Spades(2), Hearts(3), Diamonds(4);

	private int value;

	Suit(int value)
	{
		this.value = value;
	}

	public int getValue()
	{
		return value;
	}
}