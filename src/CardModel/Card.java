package CardModel;
/**
 * @author Collin Gifford
 * @author YUNHAO ZHANG
 */

/*
 * Represents a card from a standard 52-card Poker deck.
 * Has a suit from the set of Hearts, Diamonds, Clubs, and Spades,
 * and a rank of 2, 3, 4, 5, 6, 7, 8, 9, 10, Jack, Queen, King, or Ace.
 */

public class Card implements Comparable<Card>
{
	private Rank rank;
	private Suit suit;

	public Card(Rank rank, Suit suit)
	{
		this.rank = rank;
		this.suit = suit;
	}

	@Override
	public int compareTo(Card card)
	{

		if (this.rank.getValue() < card.rank.getValue())
		{
			return -1;
		} else if (this.rank.getValue() > card.rank.getValue())
		{
			return 1;
		} else
		{
			/*
			 * if (this.suit == card.suit) { throw new DuplicateCardException();
			 * }
			 */
			return 0;
		}

	}

	public Rank getRank()
	{
		return this.rank;
	}

	public Suit getSuit()
	{
		return this.suit;
	}

	@Override
	public String toString()
	{
		String rankRepresentation;
		switch (this.rank)
		{
			case Ace:
				rankRepresentation = "A";
				break;
			case King:
				rankRepresentation = "K";
				break;
			case Queen:
				rankRepresentation = "Q";
				break;
			case Jack:
				rankRepresentation = "J";
				break;
			default:
				rankRepresentation = Integer.toString(this.rank.getValue());

		}

		String suitRepresentation = "";
		switch (this.suit)
		{
			case Hearts:
				suitRepresentation = "H";
				break;
			case Diamonds:
				suitRepresentation = "D";
				break;
			case Spades:
				suitRepresentation = "S";
				break;
			case Clubs:
				suitRepresentation = "C";

		}

		return rankRepresentation + suitRepresentation;

	}

	@Override
	public int hashCode()
	{
		return ((this.rank.ordinal() << 4) | (this.suit.ordinal()));
	}

	@Override
	public boolean equals(Object o)
	{
		if (o.getClass() == this.getClass())
		{
			if (this.rank == ((Card) o).rank && this.suit == ((Card) o).suit)
			{
				return true;
			} else
			{
				return false;
			}
		} else
		{
			return false;
		}

	}

	public int getImageFileNameNumber()
	{
		if (rank == null && suit == null)
			return 0; // Special case for the back of a card

		// Aces are 1, 2, 3, 4.png
		// Kings are 5, 6, 7, 8.png
		// Queens are 10, 11, 12, 13.png
		// Deuces are 49, 50, 51, 52.png
		return suit.getValue() + (52 - (rank.getValue() - 1) * 4);

	}
}
