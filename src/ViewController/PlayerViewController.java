package ViewController;
/**
 * @Author Yunhao Zhang
 * @Author Collin Gifford
 *
 */
import java.awt.*;
import java.text.NumberFormat;
import java.util.ArrayList;

import javax.swing.*;

import AppDelegate.Dealer;
import AppDelegate.Player;
import CardModel.Card;

@SuppressWarnings("serial")
public class PlayerViewController extends JPanel
{
	boolean showCards;
	boolean needToBeAlert;
	
	JPanel bestHandPanel;

	
	JPanel holeCardPanel;
	JLabel background;

	
	Player representedPlayer;
	JLabel winnerHat;
	JLabel winnerTag;
	JLabel foldTag;
	
	JLabel currency;
	JLabel currencyShadow;
	
	public PlayerViewController(Dealer dealer, int getPlayer)
	{
		Player player = dealer.getPlayers()[getPlayer];
		//Card[] communityCards = dealer.getCommunalCards();
		this.showCards = false;
		this.representedPlayer = player;
		this.setOpaque(false);
		this.setLayout(null);
		this.setFocusable(true);
		this.needToBeAlert = true;
		//this.setBackground(Color.BLUE);
		/*
		this.setPreferredSize( new Dimension(320, 120) );
		this.setMaximumSize(new Dimension(320, 120));
		this.setMinimumSize(new Dimension(320, 120));
		*/
		
		
		/* Level 1 */
		// Set winner hat, winner tag and fold tag
		winnerHat = new JLabel(new ImageIcon("UI/crown.png"));
		winnerTag = new JLabel(new ImageIcon("UI/winnerTag.png"));
		foldTag = new JLabel(new ImageIcon("UI/folded.png"));
		
		this.add(winnerHat);
		this.add(winnerTag);
		this.add(foldTag);
		
		/* Level 2 */
		// Set avatar
		JLabel avatar;
		if(representedPlayer.getName().equals("Player"))
		{
			avatar = new JLabel(new ImageIcon("UI/player.png"));
		}
		else
		{
			avatar = new JLabel(new ImageIcon("UI/robot.png"));
		}
		this.add(avatar);
		avatar.setBounds(53, 49, 72, 72);
		
		/* Level 3 */
		// Set player name with shadow
		JLabel playerName = new JLabel(representedPlayer.getName());
		playerName.setForeground (Color.WHITE);
		JLabel playerNameShadow = new JLabel(representedPlayer.getName());
		playerName.setFont( new Font("Georgia", Font.BOLD, 12) );
		playerNameShadow.setFont( new Font("Georgia", Font.BOLD, 12) );
		this.add(playerName);
		this.add(playerNameShadow);
		playerName.setBounds(60, 79, 100, 100);
		playerNameShadow.setBounds(60, 80, 100, 100);
		
		/* Level 4 */
		// Set player balance with shadow
		NumberFormat moneyFormat = NumberFormat.getCurrencyInstance();
		this.currency = new JLabel(moneyFormat.format(representedPlayer.getBalance()));
		this.currency.setForeground (Color.WHITE);
		this.currencyShadow = new JLabel(moneyFormat.format(representedPlayer.getBalance()));
		this.currency.setFont( new Font("Georgia", Font.ITALIC, 12) );
		this.currencyShadow.setFont( new Font("Georgia", Font.ITALIC, 12) );
		this.add(this.currency);
		this.add(currencyShadow);
		this.currency.setBounds(60, 94, 100, 100);
		this.currencyShadow.setBounds(60, 95, 100, 100);
		
		
		/* Level 5 */
		// Best hand
		this.bestHandPanel = new PokerCardPanel(new ArrayList<Card>());
		this.add(bestHandPanel);
		bestHandPanel.setBounds(135, 105, 200, 50);
		
		/* Level 6 */
		// Hold cards
		
		this.holeCardPanel = new PokerCardPanel(new ArrayList<Card>());
		this.add(holeCardPanel);
		holeCardPanel.setBounds(135, 50, 200, 50);
		
		
		/* Level 7 */
		// Set background
		background = new JLabel(new ImageIcon("UI/box.png"));
		this.add(background);
		background.setBounds(0, 0, 380, 244);	
		//background.setBounds(0, 0, 276, 120);
		

	}
	
	
	/*
	 * Updates dynamic display information; Player's current balance, cards and hand
	 */
	public void updateContent(Dealer dealer, int getPlayer)
	{
		/*JLabel background = new JLabel(new ImageIcon("UI/box.png"));
		this.add(background);
		background.setBounds(0, 0, 380, 244);	
		*/
		Card[] communityCards = dealer.getCommunalCards();
		boolean isWinner = dealer.isWinner(dealer.getPlayers()[getPlayer]);
		boolean needToShowTheWinner = dealer.getShowAll() && !dealer.getPlayers()[getPlayer].hasFolded();
		showCards = dealer.getShowAll();
		
		if(dealer.getPlayers()[getPlayer].getName().equals("Player"))
		{
			showCards = true;
		}
		Player player  = dealer.getPlayers()[getPlayer];
		if(dealer.getPlayers()[getPlayer].hasFolded())
		{
			foldTag.setBounds(165, 82, 106, 45);
		}
		else
		{
			foldTag.setBounds(-212, -52, 106, 45);
		}
		
		if(isWinner && needToShowTheWinner)
		{
			//this.add(winnerHat);
			//this.add(winnerTag);
			winnerHat.setBounds(50, 15, 72, 72);
			winnerTag.setBounds(212, 52, 106, 45);
		}
		else
		{
			winnerHat.setBounds(-100, -100, 0, 0);
			winnerTag.setBounds(-100, -100, 0, 0);
		}
		
		
		//Update player balance
		NumberFormat moneyFormat = NumberFormat.getCurrencyInstance();
		String money = moneyFormat.format(this.representedPlayer.getBalance());
		if(money.substring(0,1).equals("("))
		{
			money = "-" + money.substring(1, money.length() - 1);
			currency.setForeground (Color.RED);
			this.currency.setText(money);
			this.currencyShadow.setText(money);
			if(needToBeAlert && player.getName().equals("Player"))
			{
				ImageIcon alertIcon = new ImageIcon("UI/alert.png");
				JOptionPane.showMessageDialog(null,
				          "Alert:\nLooks like you are gambling in debt. You can keep playing, but just remember:\n\"By gaming we lose both our time and treasure: two things most precious to the life of man.\"", "Alert",
				          JOptionPane.ERROR_MESSAGE, alertIcon);
				this.needToBeAlert = false;
			}
		}
		else
		{
			currency.setForeground (Color.WHITE);
			this.needToBeAlert = true;
			this.currency.setText(money);
			this.currencyShadow.setText(money);
		}
		
		
		
		//Update hole cards panel
		this.remove(holeCardPanel);
		ArrayList<Card> holdCard = new ArrayList<Card>();
		for(int i = 0; i < representedPlayer.getHoleCards().length; i++)
		{
			if (showCards)
			holdCard.add(representedPlayer.getHoleCards()[i]);
			else holdCard.add(new Card(null, null));
		}
		
		if (!(representedPlayer.hasFolded()))
		holeCardPanel = new PokerCardPanel(holdCard);
		else
			holeCardPanel = new PokerCardPanel(new ArrayList<Card>());
		this.add(holeCardPanel);
		holeCardPanel.setBounds(135, 50, 200, 50);
		
		
		//Update best hand panel
		this.remove(bestHandPanel);
		if (showCards && !(representedPlayer.hasFolded()))
		bestHandPanel = new PokerCardPanel(representedPlayer.getBestHand(communityCards).toArrayList());
		else
		bestHandPanel = new PokerCardPanel(new ArrayList<Card>());
		this.add(bestHandPanel);
		bestHandPanel.setBounds(135, 105, 200, 50);
		this.add(background);
		background.setBounds(0, 0, 380, 244);	
		
		this.repaint();
		
		
		
	}
}
