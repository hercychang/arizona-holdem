/*package ViewController;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;

import javax.swing.*;
import java.io.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.imageio.ImageIO;
import java.util.ArrayList;

import ViewController.PokerCardPanel;
import AppDelegate.Dealer;
import AppDelegate.Player;
import CardModel.Card;
import Main.GameInstance;

public class HoldemGUI extends JFrame
{
	
	// music control
	private JButton musicPlayer;
	private JButton play;
	private static boolean playMusic;
	private static Clip clip;
	
	// game
	private JPanel holeCardPanel;
	private Dealer game;
	private String getOutput;
	
	public static void main(String [] args) throws Exception
	{
		playMusic = true;
		// setup window
		JFrame UIWindow = new HoldemGUI();
		UIWindow.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		UIWindow.setVisible(true);
		UIWindow.setSize(1024, 768);
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		UIWindow.setLocation(screenSize.width/2 - UIWindow.getSize().width/2, screenSize.height/2 - UIWindow.getSize().height/2);
		UIWindow.setResizable(false);
		UIWindow.setTitle("Arizona Hold 'em");
		
		
		// setup background music
		AudioInputStream inputStream = AudioSystem.getAudioInputStream(new File("src/music.wav"));
        clip = AudioSystem.getClip();
        clip.open(inputStream);
        clip.loop(Clip.LOOP_CONTINUOUSLY);
        //Thread.sleep(-1);//10000); // looping as long as this thread is alive
		
	}


	public HoldemGUI()
	{
		
		setLayout(new GridLayout(2, 2, 4, 4));
		
		//setLayout(new BorderLayout());
		//setLayout(new FlowLayout());
		
		//setContentPane(new JLabel(new ImageIcon("UI/background.jpg")));
		
		// setup musicPlayer
		musicPlayer = new JButton("Stop Music");
		add(musicPlayer);
		musicPlayer.addActionListener(new musicController());

		game = new Dealer(new Player []{
				new Player("You"),
				new Player("Robot 1"),
				new Player("Robot 2"),
				new Player("Robot 3")});
		
		getOutput = game.deal();
		
		ArrayList<Card> holdCard = new ArrayList<Card>();
		for(int i = 0; i < game.players[0].getHoleCards().length; i++)
		{
			holdCard.add(game.players[0].getHoleCards()[i]);
		}
		
		

		//test print out
		System.out.println(getOutput);
				
		
		
		holeCardPanel = new PokerCardPanel(holdCard);
		this.add(holeCardPanel);
		
		play = new JButton("Play");
		add(play);
		play.addActionListener(new startARound());
		//this.add(fiveCardPanel);

		
	}
	

	private class musicController implements ActionListener
	{
		public void actionPerformed(ActionEvent e)
		{
			if(playMusic)
			{
				clip.stop();
				playMusic = false;
				musicPlayer.setText("Play Music");
			}
			else
			{
				clip.loop(Clip.LOOP_CONTINUOUSLY);
				playMusic = true;
				musicPlayer.setText("Stop Music");
			}
		}
	}
	
	private class startARound implements ActionListener
	{
		public void actionPerformed(ActionEvent e)
		{
			getOutput = game.deal();
			
			//test print out
			System.out.println(getOutput);
			
			ArrayList<Card> holdCard = new ArrayList<Card>();
			for(int i = 0; i < game.players[0].getHoleCards().length; i++)
			{
				holdCard.add(game.players[0].getHoleCards()[i]);
			}

			
		}
	}
	
}
*/