package ViewController;

/**
 * @Author Yunhao Zhang
 * @Author Collin Gifford
 *
 */
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;

import javax.swing.*;

import com.apple.eawt.Application;

import AppDelegate.Dealer;
import AppDelegate.Player;

@SuppressWarnings("serial")
public class MainViewController extends JFrame
{
	// Music player properties
	private JButton musicPlayer;
	private static boolean musicIsPlaying;
	private static Clip clip;
	private ImageIcon errorIcon = new ImageIcon("UI/error.png");
	public String COPYRIGHT = "Copyright \u00a9 2014 Hercy & Collin's Poker Company. All rights reserved.";
	public String VERSION = "v1.3.1 (Beta)";

	private Dealer game;
	private GameViewController gameContent;
	private JTextField betField;
	// private String getOutput;

	private static boolean acceptingBets;

	private static boolean awaitingDeal;

	private static MainViewController destatic = null;

	public static void main(String[] args) throws Exception
	{
		// Setup the application window
		@SuppressWarnings("unused")
		MainViewController UIWindow = new MainViewController();

		// Setup background music
		AudioInputStream inputStream = AudioSystem
				.getAudioInputStream(new File("src/music.wav"));
		clip = AudioSystem.getClip();
		clip.open(inputStream);
		clip.loop(Clip.LOOP_CONTINUOUSLY);
		// Thread.sleep(-1);//10000); // looping as long as this thread is alive

		/*
		 * UIWindow.game.deal(); UIWindow.gameContent.updateContent();
		 */

		/*
		 * Game Loop
		 */
		/*
		 * while (true) {
		 * 
		 * 
		 * UIWindow.game.preBetDeal(); UIWindow.gameContent.botBets();
		 * UIWindow.gameContent.updateContent();
		 * 
		 * waitForBet(); UIWindow.game.postBetDeal();
		 * UIWindow.gameContent.updateContent();
		 * 
		 * UIWindow.gameContent.updateContent();
		 * 
		 * }
		 */
		awaitingDeal = true;

	}

	/*
	 * private static void waitForDeal() { MainViewController.awaitingDeal =
	 * true; while (MainViewController.awaitingDeal) { if (!(awaitingDeal))
	 * return; } }
	 */
	@SuppressWarnings("static-access")
	public MainViewController()
	{

		MainViewController.destatic = this;
		this.game = new Dealer(new Player[]
		{ new Player("Player"), new Player("Robot 1"), new Player("Robot 2"),
				new Player("Robot 3") });

		this.acceptingBets = false;
		this.awaitingDeal = false;

		// this.setContentPane(new JLabel(new ImageIcon("UI/background.jpg")));

		// Setup layout
		this.setLayout(null);

		// Setup copyright & version info
		JLabel copyrightInfo = new JLabel(COPYRIGHT);
		this.add(copyrightInfo);
		copyrightInfo.setHorizontalAlignment(SwingConstants.LEFT);
		copyrightInfo.setFont(new Font("Georgia", Font.PLAIN, 10));
		copyrightInfo.setForeground(Color.WHITE);
		copyrightInfo.setBounds(10, 680, 1024, 100);

		JLabel versionInfo = new JLabel(VERSION);
		this.add(versionInfo);
		versionInfo.setHorizontalAlignment(SwingConstants.RIGHT);
		versionInfo.setFont(new Font("Georgia", Font.PLAIN, 10));
		versionInfo.setForeground(Color.WHITE);
		versionInfo.setBounds(-10, 680, 1024, 100);

		/* 1st Level */
		// Setup music player
		musicPlayer = new JButton();
		musicIsPlaying = true;
		musicPlayer.setIcon(new ImageIcon("UI/mute.png"));
		musicPlayer.setBorder(BorderFactory.createEmptyBorder());
		musicPlayer.setContentAreaFilled(false);
		this.add(musicPlayer);
		musicPlayer.setBounds(40, 30, 100, 100);
		musicPlayer.addActionListener(new musicController());

		/* 2nd Level */
		// Setup quite button
		JButton quitbutton = new JButton();
		quitbutton.setIcon(new ImageIcon("UI/kill.png"));
		quitbutton.setBorder(BorderFactory.createEmptyBorder());
		quitbutton.setContentAreaFilled(false);
		quitbutton.addActionListener(new QuitListener());
		quitbutton.setBounds(884, 30, 100, 100);
		this.add(quitbutton);

		/* 3rd Level */
		// Setup deal hand button and background
		JButton dealbutton = new JButton();
		dealbutton.setIcon(new ImageIcon("UI/dealHandButton.png"));
		dealbutton.setBorder(BorderFactory.createEmptyBorder());
		dealbutton.setContentAreaFilled(false);
		dealbutton.setHorizontalAlignment(SwingConstants.CENTER);
		dealbutton.addActionListener(new DealListener());
		dealbutton.setBounds(437, 440, 150, 80);
		this.add(dealbutton);

		JLabel dealHandBg = new JLabel(new ImageIcon("UI/dealHandBg.png"));
		this.add(dealHandBg);
		dealHandBg.setBounds(448, 456, 128, 50);

		/* 4th Level */
		// Setup fold button
		JButton foldButton = new JButton();
		foldButton.setIcon(new ImageIcon("UI/foldButton.png"));
		foldButton.setContentAreaFilled(false);
		foldButton.setBorder(BorderFactory.createEmptyBorder());
		foldButton.setHorizontalAlignment(SwingConstants.CENTER);
		// foldButton.setForeground(Color.WHITE);
		// foldButton.setFont(new Font("Georgia", Font.BOLD, 24));
		foldButton.addActionListener(new FoldListener());
		this.add(foldButton);
		foldButton.setBounds(247, 645, 100, 73);

		JLabel foldBg = new JLabel(new ImageIcon("UI/fold.png"));
		this.add(foldBg);
		foldBg.setBounds(247, 645, 100, 73);

		/* 5th Level */
		// Setup textField
		betField = new JTextField("($5.00 or more)");
		this.add(betField);
		betField.setBounds(683, 650, 90, 35);

		/* 6th Level */
		// Setup call button
		JButton callButton = new JButton();
		callButton.setIcon(new ImageIcon("UI/callButton.png"));
		callButton.setContentAreaFilled(false);
		callButton.setBorder(BorderFactory.createEmptyBorder());
		callButton.setHorizontalAlignment(SwingConstants.CENTER);
		// callButton.setForeground(Color.WHITE);
		// callButton.setFont(new Font("Georgia", Font.BOLD, 20));
		callButton.addActionListener(new BetListener());
		this.add(callButton);
		callButton.setBounds(678, 679, 100, 35);

		JLabel callBg = new JLabel(new ImageIcon("UI/call.png"));
		this.add(callBg);
		callBg.setBounds(678, 645, 100, 73);

		/* 5th Level */
		// Setup game conent
		// Set location and size: SomeObject.setBounds(x, y, width, height);
		// Set JPanel transparent: SomeJPanel.setOpaque(false)
		gameContent = new GameViewController(game);

		this.add(gameContent);
		gameContent.setLocation(0, 0);
		// gameContent.setSize(1024,746);

		/* 6th Level */
		// Setup game background
		JLabel background = new JLabel(new ImageIcon("UI/background.jpg"));
		this.add(background);
		background.setBounds(0, 0, 1024, 768);

		this.pack();
		this.setLocationRelativeTo(null);

		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		this.setSize(1024, 768);
		this.setResizable(false);
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		this.setLocation(screenSize.width / 2 - this.getSize().width / 2,
				screenSize.height / 2 - this.getSize().height / 2);
		this.setTitle("Arizona Hold 'em");

		this.setIconImage(new ImageIcon("UI/icon.png").getImage());
		Application application = Application.getApplication();
		Image image = Toolkit.getDefaultToolkit().getImage("UI/icon.png");
		application.setDockIconImage(image);
		application.setDockIconBadge("Beta");

		this.setVisible(true);

	}

	// Music play button action listener
	private class musicController implements ActionListener
	{
		public void actionPerformed(ActionEvent e)
		{
			if (musicIsPlaying)
			{
				clip.stop();
				musicIsPlaying = false;
				// musicPlayer.setText("Play Music");
				musicPlayer.setIcon(new ImageIcon("UI/play.png"));
			} else
			{
				clip.loop(Clip.LOOP_CONTINUOUSLY);
				musicIsPlaying = true;
				// musicPlayer.setText("Stop Music");
				musicPlayer.setIcon(new ImageIcon("UI/mute.png"));
			}
		}
	}

	private class QuitListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e)
		{
			System.exit(0);
		}
	}

	private class FoldListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e)
		{
			if (acceptingBets)
			{

				MainViewController.acceptingBets = false;
				destatic.gameContent.playerFold();
				destatic.game.postBetDeal();
				destatic.gameContent.updateContent(game);
				awaitingDeal = true;
			}
		}
	}

	private class BetListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e)
		{
			//if (!game.getPlayers()[0].hasFolded() && !game.getShowAll() )
			if(acceptingBets)
			{
				acceptingBets = false;
				try
				{
					double amount = Double.parseDouble(betField.getText());
					String amountFormatChecker = "" + amount;
					String amountFormatAfterFloat = null;
					for (int i = 0; i < amountFormatChecker.length(); i++)
					{
						if (amountFormatChecker.substring(i, i + 1).equals("."))
						{
							amountFormatAfterFloat = amountFormatChecker.substring(i);
						}
					}
					if (amountFormatAfterFloat.length() > 3)
					{
						JOptionPane
								.showMessageDialog(
										null,
										"Error:\nPlease enter an amount to the tenths place, and no further.\nExample: \"8.00\", \"94.6\",  \"29\"",
										"Error Massage",
										JOptionPane.ERROR_MESSAGE);
					}

					if (amount >= 5)
					{
						MainViewController.acceptingBets = false;
						destatic.gameContent.playerBet(amount);
						destatic.game.postBetDeal();
						destatic.gameContent.updateContent(game);
						awaitingDeal = true;
					} else
					{
						JOptionPane
								.showMessageDialog(
										null,
										"Error: Please enter an amount of at least $5!",
										"Error Massage",
										JOptionPane.ERROR_MESSAGE, errorIcon);
					}
				} catch (NumberFormatException ex)
				{

					JOptionPane
							.showMessageDialog(
									null,
									"Error: Please enter only numbers.\nExample: \"8.00\", \"94.6\",  \"29\"",
									"Error Massage", JOptionPane.ERROR_MESSAGE,
									errorIcon);

				} catch (NullPointerException en)
				{

					/*
					 * JOptionPane.showMessageDialog(null,
					 * "Error:\nPlease press \"Deal Hand\" button first to start the game."
					 * , "Error Massage", JOptionPane.ERROR_MESSAGE, errorIcon);
					 */

				}

			}

		}

	}

	private class DealListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e)
		{
			if (MainViewController.awaitingDeal)
			{

				MainViewController.awaitingDeal = false;
				destatic.game.preBetDeal();
				destatic.gameContent.botBets();
				destatic.gameContent.updateContent(game);
				acceptingBets = true;
				/*
				 * destatic.game.postBetDeal();
				 * destatic.gameContent.updateContent();
				 * 
				 * destatic.gameContent.updateContent(); awaitingDeal = true;
				 */
			}

		}
	}

}
