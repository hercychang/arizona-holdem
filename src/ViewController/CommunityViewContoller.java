package ViewController;
/**
 * @Author Yunhao Zhang
 * @Author Collin Gifford
 *
 */
import java.awt.*;
import java.text.NumberFormat;
import java.util.ArrayList;

import javax.swing.*;

import AppDelegate.Dealer;

import CardModel.Card;


@SuppressWarnings("serial")
public class CommunityViewContoller extends JPanel
{
	private JLabel bank;
	private JLabel bankShadow;
	
	private Dealer dealer;
	private PokerCardPanel communityCardPanel;
	
	
	public CommunityViewContoller(Dealer dealer)
	{
		this.dealer = dealer;
		double pot = this.dealer.getPot();
		
		this.setOpaque(false);
		this.setLayout(null);
		this.setFocusable(true);
		//this.setBackground(Color.BLUE);
		
		/* 1st Level */
		// Setup bank label and shadow
		JLabel bankLabel = new JLabel("- POT -");
		JLabel bankLabelShadow = new JLabel("- POT -");
		bankLabel.setForeground (Color.ORANGE);
		bankLabel.setHorizontalAlignment( SwingConstants.CENTER);
		bankLabelShadow.setHorizontalAlignment( SwingConstants.CENTER);
		bankLabel.setFont( new Font("Georgia", Font.PLAIN, 16) );
		bankLabelShadow.setFont( new Font("Georgia", Font.PLAIN, 16) );
		this.add(bankLabel);
		this.add(bankLabelShadow);
		bankLabel.setBounds(0, -20, 220, 50);
		bankLabelShadow.setBounds(0, -19, 220, 50);
		
		/* 2nd Level */
		// Setup pot label and shadow
		NumberFormat moneyFormat = NumberFormat.getCurrencyInstance();
		bank = new JLabel(moneyFormat.format(pot));
		bank.setForeground (Color.WHITE);
		bankShadow = new JLabel(moneyFormat.format(pot));
		bank.setHorizontalAlignment( SwingConstants.CENTER);
		bankShadow.setHorizontalAlignment( SwingConstants.CENTER);
		bank.setFont( new Font("Georgia", Font.PLAIN, 24) );
		bankShadow.setFont( new Font("Georgia", Font.PLAIN, 24) );
		this.add(bank);
		this.add(bankShadow);
		bank.setBounds(0, -20, 220, 100);
		bankShadow.setBounds(0, -19, 220, 100);
		
		/* 3rd Level */
		// Setup community card label and shadow
		JLabel communityLabel = new JLabel("- COMMUNITY CARDS -");
		JLabel communityShadow = new JLabel("- COMMUNITY CARDS -");
		communityLabel.setForeground (Color.ORANGE);
		communityLabel.setHorizontalAlignment( SwingConstants.CENTER);
		communityShadow.setHorizontalAlignment( SwingConstants.CENTER);
		communityLabel.setFont( new Font("Georgia", Font.PLAIN, 16) );
		communityShadow.setFont( new Font("Georgia", Font.PLAIN, 16) );
		this.add(communityLabel);
		this.add(communityShadow);
		communityLabel.setBounds(0, 65, 220, 50);
		communityShadow.setBounds(0, 66, 220, 50);
		
		/* 4th Level */
		// Setup community card panel
		ArrayList <Card> empty = new ArrayList<Card>();
		empty.add(new Card(null, null));
		empty.add(new Card(null, null));
		empty.add(new Card(null, null));
		empty.add(new Card(null, null));
		empty.add(new Card(null, null));
		this.communityCardPanel = new PokerCardPanel(empty);
		this.add(communityCardPanel);
		communityCardPanel.setBounds(19, 105, 200, 50);
	}
	
	/*
	 * Update dynamic fields; current pot value, community cards
	 */
	public void updateContent()
	{
		//Update pot label
		NumberFormat moneyFormat = NumberFormat.getCurrencyInstance();
		bank.setText(moneyFormat.format(dealer.getPot()));
		bankShadow.setText(moneyFormat.format(dealer.getPot()));
		
		//Update community cards display
		this.remove(communityCardPanel);
		ArrayList<Card> community = new ArrayList<Card>();
		for(int i = 0; i < dealer.getCommunalCards().length; i++)
		{
			community.add(dealer.getCommunalCards()[i]);
		}
		this.communityCardPanel = new PokerCardPanel(community);
		this.add(communityCardPanel);
		communityCardPanel.setBounds(19, 105, 200, 50);
		
	}
	
	
}
