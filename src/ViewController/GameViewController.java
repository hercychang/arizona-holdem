package ViewController;
/**
 * @Author Yunhao Zhang
 * @Author Collin Gifford
 *
 */
import java.awt.*;

import java.util.Random;

import javax.swing.*;

import AppDelegate.Dealer;

@SuppressWarnings("serial")
public class GameViewController extends JPanel
{
	PlayerViewController user, bot1, bot2, bot3;
	
	CommunityViewContoller commView;
	
	Dealer gameDriver;
	
	public GameViewController(Dealer dealer)
	{
		this.gameDriver = dealer;
		
		//JLabel hello = new JLabel("Hello");
		//this.add(hello);
		this.setOpaque(false);
		this.setLayout(null);
		//this.setSize(1024,746);
		this.setSize(1024,768);

		
		user = new PlayerViewController(gameDriver, 0);
		bot1 = new PlayerViewController(gameDriver, 1);
		bot2 = new PlayerViewController(gameDriver, 2);
		bot3 = new PlayerViewController(gameDriver, 3);
		
		this.commView = new CommunityViewContoller(gameDriver);
		
		JPanel north = bot2,
				south = user,
				west = bot1,
				east = bot3,
				center = commView;
		/*
		north.setBounds(374, 20, 276, 120);
		south.setBounds(374, 606, 276, 120);
		west.setBounds(20, 324, 276, 120);
		east.setBounds(728, 324, 276, 120);
		center.setBounds(0, 0, 276, 120);
		*/
		
		north.setBounds(322, -10, 380, 244);
		south.setBounds(322, 552, 380, 244);
		west.setBounds(-10, 270, 380, 244);
		east.setBounds(654, 270, 380, 244);
		center.setBounds(402, 253, 220, 220);
		
		this.add(north, BorderLayout.NORTH);
		this.add(south, BorderLayout.SOUTH);
		this.add(west, BorderLayout.WEST);
		this.add(east, BorderLayout.EAST);
		this.add(center, BorderLayout.CENTER);
		
		
				
	}

	public void updateContent(Dealer dealer) 
	{
		this.commView.updateContent();
		this.user.updateContent(gameDriver, 0);
		this.bot1.updateContent(gameDriver, 1);
		this.bot2.updateContent(gameDriver, 2);
		this.bot3.updateContent(gameDriver, 3);
	}
	
	
	
	public void playerFold()
	{
		user.representedPlayer.setFolded(true);
	}

	public void playerBet(double amount) 
	{
		//Unnecessary but for double surety
		user.representedPlayer.setFolded(false);
		user.representedPlayer.take(amount);
		gameDriver.addToPot(amount);
		
	}

	public void botBets() {
		
		
		//bot 1 decision
		if (new Random().nextBoolean())
		{
			bot1.representedPlayer.setFolded(false);
			bot1.representedPlayer.take(5.0);
			gameDriver.addToPot(5.0);
		}
		else
		{
			bot1.representedPlayer.setFolded(true);
		}
		
		
		//bot 2 decision
		if (new Random().nextBoolean())
		{
			bot2.representedPlayer.setFolded(false);
			bot2.representedPlayer.take(5.0);
			gameDriver.addToPot(5.0);
		}
		else
		{
			bot2.representedPlayer.setFolded(true);
		}
		
		
		//bot 3 decision
		if (new Random().nextBoolean())
		{
			bot3.representedPlayer.setFolded(false);
			bot3.representedPlayer.take(5.0);
			gameDriver.addToPot(5.0);
		}
		else
		{
			bot3.representedPlayer.setFolded(true);
		}
		
	}
	
	
	
}
