package ViewController;
/**
 * @Author Yunhao Zhang
 * @Author Collin Gifford
 *
 */
//This JPanel requires a list of Card objects that now have method getImageFileNameNumber
//that returns an integer in the range of 0 (the back of a card) through 52 (the 2 of Diamonds)
//It is assumed the card image file names follow the pattern shown in CardTest
//
//@author Rick Mercer 

//Modified on 4-Feb in section by:
//1. 
//2.


import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;
import javax.swing.JPanel;

import CardModel.Card;

@SuppressWarnings("serial")
public class PokerCardPanel extends JPanel
{

	private List<Card> cards;
	private List<Image> images;

	public PokerCardPanel(List<Card> cardListToShow)
	{
		//this.setBackground(Color.GREEN);
		this.setOpaque(false);
		cards = new ArrayList<Card>(cardListToShow);
		images = new ArrayList<Image>(53);
		try
		{
			Image image = null;
			for (int cardFileName = 0; cardFileName <= 52; cardFileName++)
			{
				String fileName = "cardImages/" + cardFileName + ".png";
				image = ImageIO.read(new File(fileName));
				images.add(image);
			}
		} catch (IOException e)
		{
			e.printStackTrace();
		}
	}

	// Override the paintComponent method in JPanel
	@Override
	public void paintComponent(Graphics g)
	{
		Graphics2D g2 = (Graphics2D) g;
		
		// TODO: Draw the list of images onto this JPanel.
		int x = 0;
		for(Card card : cards)
		{
			g2.drawImage(images.get(card.getImageFileNameNumber()), x, 0, 37, 50, null);
			x += 37;
		}

		/*
		 * Note: This JPanels constructor initialized the List of Image objects
		 * so images.get(1) will return the image of the Ace of clubs and
		 * images.get(52) will return the image of the 2 of Diamonds
		 * 
		 * You will also need the new method getImageFileNameNumber added to
		 * model.Card:
		 * 
		 * Card c1 = new Card(Rank.Deuce, Suit.Clubs); Card c2 = new
		 * Card(Rank.Deuce, Suit.Spades); Card c3 = new Card(Rank.Deuce,
		 * Suit.Hearts); Card c4 = new Card(Rank.Deuce, Suit.Diamonds);
		 * assertEquals(49, c1.getImageFileNameNumber()); assertEquals(50,
		 * c2.getImageFileNameNumber()); assertEquals(51,
		 * c3.getImageFileNameNumber()); assertEquals(52,
		 * c4.getImageFileNameNumber());
		 */

	}
	
}
