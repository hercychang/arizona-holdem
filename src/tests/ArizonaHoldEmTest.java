package tests;
/**
 * @author Collin Gifford
 * @author YUNHAO ZHANG
 */
/*
import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Test;

import AppDelegate.Dealer;
import AppDelegate.Player;
import AppDelegate.PokerHand;
import CardModel.Card;
import CardModel.Rank;
import CardModel.Suit;


public class ArizonaHoldEmTest
{

	@Test
	public void testAnte()
	{
		Player player = new Player("Bob");
		player.ante();
		assertEquals(98.0, player.getBalance(), 0.001);
	}

	@Test
	public void testDeposit()
	{
		Player player = new Player("Charlie");
		player.deposit(50.0);
		assertEquals(150.0, player.getBalance(), 0.001);

	}

	@Test
	public void testDealHoleCards()
	{
		Player player = new Player("Dennis");
		player.dealHoleCards(new Card(Rank.Eight, Suit.Clubs), new Card(
				Rank.Ten, Suit.Diamonds));
		assertTrue(player.getHoleCards()[0].compareTo(new Card(Rank.Eight,
				Suit.Clubs)) == 0);
		assertTrue(player.getHoleCards()[1].compareTo(new Card(Rank.Ten,
				Suit.Diamonds)) == 0);
	}

	@Test
	public void testGetBestHandRoyalFlush()
	{
		Card HA, HK, HQ, HJ, H10, CA, DA;
		HA = new Card(Rank.Ace, Suit.Hearts);
		HK = new Card(Rank.King, Suit.Hearts);
		HQ = new Card(Rank.Queen, Suit.Hearts);
		HJ = new Card(Rank.Jack, Suit.Hearts);
		H10 = new Card(Rank.Ten, Suit.Hearts);
		CA = new Card(Rank.Ace, Suit.Clubs);
		DA = new Card(Rank.Ace, Suit.Diamonds);

		Card[] community = { DA, HK, HQ, HJ, H10 };

		Player player = new Player("Eleanor");
		player.dealHoleCards(CA, HA);

		assertTrue(player.getBestHand(community).equals(new PokerHand(HA, HK, HQ, HJ, H10)));

	}

	@Test
	public void testFindWinningHand()
	{
		Card[] community = {
							new Card(Rank.Five, Suit.Clubs),
							new Card(Rank.Five, Suit.Diamonds),
							new Card(Rank.Four, Suit.Diamonds),
							new Card(Rank.Six, Suit.Diamonds),
							new Card(Rank.Ace, Suit.Hearts)
							};
		Player[] players =
		{ new Player("Yolanda"), new Player("Johann"), new Player("Khan") };
		players[0].dealHoleCards(new Card(Rank.Three, Suit.Diamonds), new Card(
				Rank.Seven, Suit.Diamonds));
		players[1].dealHoleCards(new Card(Rank.Ace, Suit.Clubs), new Card(
				Rank.Ace, Suit.Spades));
		players[2].dealHoleCards(new Card(Rank.Five, Suit.Spades), new Card(
				Rank.Five, Suit.Hearts));

		Dealer dealer = new Dealer(players);
		dealer.setCommunityCards(community);
		assertTrue(dealer.getWinningHands().get(dealer.getWinningHands().size() - 1).compareTo(new PokerHand(
				new Card(Rank.Three, Suit.Diamonds), new Card(Rank.Seven,
						Suit.Diamonds), new Card(Rank.Five, Suit.Diamonds),
				new Card(Rank.Four, Suit.Diamonds), new Card(Rank.Six,
						Suit.Diamonds))) == 0);
		assertTrue(dealer.getWinningHands().size() == 1);
		assertTrue(dealer.getWinners().size() == 1);
		assertTrue(dealer.getWinners().get(0).equals(players[0]));

	}
	
	@Test
	public void testFindWinningHandWith2Winners()
	{
		Card[] community = {
							new Card(Rank.Five, Suit.Clubs),
							new Card(Rank.Three, Suit.Diamonds),
							new Card(Rank.Four, Suit.Diamonds),
							new Card(Rank.Six, Suit.Diamonds),
							new Card(Rank.Ace, Suit.Hearts)
							};
		Player[] players = {new Player("Yolanda"),
							new Player("Johann"),
							new Player("Khan")};
		players[0].dealHoleCards(new Card(Rank.Three, Suit.Hearts), new Card(Rank.Ace, Suit.Diamonds));
		players[1].dealHoleCards(new Card(Rank.Three, Suit.Clubs), new Card(Rank.Ace, Suit.Spades));
		players[2].dealHoleCards(new Card(Rank.Three, Suit.Spades), new Card(Rank.Ace, Suit.Clubs));

		Dealer dealer = new Dealer(players);
		dealer.setCommunityCards(community);
		
		assertTrue(dealer.getWinningHands().size() == 3);
		assertTrue(dealer.getWinners().size() == 3);

	}

	@Test
	public void testAwardWinnings()
	{
		Player[] players = {new Player("Alice"), new Player("Bob")};
		ArrayList<Player> playerslist = new ArrayList<Player>();
		
		playerslist.add(players[0]);
		playerslist.add(players[1]);

		Dealer dealer = new Dealer(players);
		dealer.awardWinnings(100.0, playerslist);
		assertEquals(playerslist.get(0).getBalance(), 150.0, 0.001);
		assertEquals(playerslist.get(1).getBalance(), 150.0, 0.001);
	}

	@Test
	public void testNoDuplicateCardsDealt()
	{
		Player[] players =
		{ new Player("Alice"), new Player("Bob"), new Player("Charlie") };
		Dealer dealer = new Dealer(players);
		for (int i = 0; i < 1000; i++)
		{
			dealer.deal();
			java.util.Set<Card> dealtCards = new java.util.HashSet<Card>(11);
			for (Player player : players)
			{
				for (Card card : player.getHoleCards())
				{
					if (!(dealtCards.add(card)))
					{
						fail("Duplicate cards dealt!");
					}
				}
			}
			for (Card card : dealer.getCommunalCards())
			{
				if (!(dealtCards.add(card)))
				{
					fail("Duplicate cards dealt!");
				}
			}
		}
	}

	@Test
	public void testPlayerString()
	{
		Player player = new Player("Bertha");
		player.dealHoleCards(new Card(Rank.Five, Suit.Clubs), new Card(Rank.Ace, Suit.Hearts));
		assertTrue(player.getName().equals("Bertha"));
		System.out.print(player.toString());
	}

	@Test
	public void testGetPlayerName()
	{
		Player player = new Player("Bertha");
		assertTrue(player.getName().equals("Bertha"));
	}

}
*/